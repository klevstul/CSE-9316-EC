/*
 * DAOFactory.java
 *
 * Created on 9 August 2003, 14:36
 */
package com.myPhonebook.dao;

import com.myPhonebook.dao.support.PhonebookDAOImpl;
import com.myPhonebook.dao.support.UserDAOImpl;
import com.myPhonebook.dao.support.NewsDAOImpl;
import java.util.HashMap;
import java.util.Map;
/**
 *
 * @author  yunki
 */
public class DAOFactory
{	
	private static final String USER_DAO = "userDAO";
	
	private static final String PHONEBOOK_DAO = "phoneBookDAO";
	
	private Map daos;
	
	private static DAOFactory instance = new DAOFactory();
	
	/** Creates a new instance of DAOFactory */
	private DAOFactory()
	{
		daos = new HashMap();
		daos.put(USER_DAO, new UserDAOImpl());
		daos.put(PHONEBOOK_DAO, new PhonebookDAOImpl());
		//this line added by nosh
		daos.put(NEWS_DAO, new NewsDAOImpl());
	}
	
	public UserDAO getUserDAO()
	{
		return (UserDAO) daos.get(USER_DAO);
	}

	public PhonebookDAO getPhonebookDAO()
	{
		return (PhonebookDAO) daos.get(PHONEBOOK_DAO);
	}
	//this getter added by nosh
	public UserDAO getNewsDAO()
	{
		return (NewsDAO) daos.get(NEWS_DAO);
	}
	
	public static DAOFactory getInstance()
	{
		return instance;
	}
}