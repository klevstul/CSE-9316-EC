/*
 * LogoutCommand.java
 *
 * Created on 11 August 2003, 12:37
 */

package com.myPhonebook.web;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author  yunki
 */
public class LogoutCommand implements Command {
	
	/** Creates a new instance of LogoutCommand */
	public LogoutCommand() {
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("The operation list was requested");
		out.close();	
		return null;
	}
	
}
