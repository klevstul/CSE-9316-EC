/*
 * ListCommand.java
 *
 * Created on 9 August 2003, 11:46
 */

package com.myPhonebook.web;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This command finds all the phone book entries for a particular user
 * unless they are admin
 * @author  yunki
 */
public class ListCommand implements Command {
	
	/** Creates a new instance of ListCommand */
	public ListCommand() {
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("The operation list was requested");
		out.close();	
		
		return null;
	}
	
}
