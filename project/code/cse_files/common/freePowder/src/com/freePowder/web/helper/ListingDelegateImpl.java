/*
 * file:	ListingDelegateImpl.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	08. Nov 2003
 *
 */

package com.freePowder.web.helper;

import com.freePowder.business.ListingService;
import com.freePowder.business.support.ListingServiceImpl;




public class ListingDelegateImpl extends ListingDelegate {

	private static ListingDelegateImpl instance = new ListingDelegateImpl();
	
	private ListingService service;
	
	private ListingDelegateImpl() {
		service = new ListingServiceImpl();
	}
	
	public static ListingDelegate getInstance() {
		return instance;
	}

	protected ListingService getListingService() {
		return service;
	}

}
