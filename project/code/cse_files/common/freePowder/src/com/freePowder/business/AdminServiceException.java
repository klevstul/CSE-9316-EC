/*
 * file:	AdminServiceException.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	08. Nov 2003
 *
 */

package com.freePowder.business;

import com.freePowder.common.NestedException;



public class AdminServiceException extends NestedException {


	public AdminServiceException(String message) {
		super(message);
	}


	public AdminServiceException(String message, Throwable cause) {
		super(message, cause);
	}


	public AdminServiceException(Throwable cause) {
		super(cause);
	}

}
