/*
 * file:	DAOFactory.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	02. Nov 2003
 *
 */

package com.freePowder.dao;

import java.util.HashMap;
import java.util.Map;

import com.freePowder.dao.support.UserDAOImpl;
import com.freePowder.dao.support.NewsDAOImpl;
import com.freePowder.dao.support.WeatherDAOImpl;
import com.freePowder.dao.support.SalesLogDAOImpl;
import com.freePowder.dao.support.SupplierDAOImpl;
import com.freePowder.dao.support.BrowseDAOImpl;



public class DAOFactory
{	
	private static final String USER_DAO = "userDAO";
	private static final String NEWS_DAO = "newsDAO";
	private static final String WEATHER_DAO = "weatherDAO";
	private static final String SUPPLIER_DAO = "supplierDAO";
	private static final String BROWSE_DAO = "browseDAO";
	
	private Map daos;
	
	private static DAOFactory instance = new DAOFactory();
	
	/** Creates a new instance of DAOFactory */
	private DAOFactory()
	{
		daos = new HashMap();
		daos.put(USER_DAO, new UserDAOImpl());
		daos.put(NEWS_DAO, new NewsDAOImpl());
		daos.put(WEATHER_DAO, new WeatherDAOImpl());
		daos.put(SUPPLIER_DAO, new SupplierDAOImpl());
		daos.put(BROWSE_DAO, new BrowseDAOImpl());
	}
	
	public UserDAO getUserDAO()
	{
		return (UserDAO) daos.get(USER_DAO);
	}

	public NewsDAO getNewsDAO()
	{
		return (NewsDAO) daos.get(NEWS_DAO);
	}
	
	public WeatherDAO getWeatherDAO()
	{
		return (WeatherDAO) daos.get(WEATHER_DAO);
	}

	public SupplierDAO getSupplierDAO()
	{
		return (SupplierDAO) daos.get(SUPPLIER_DAO);
	}

	public BrowseDAO getBrowseDAO()
	{
		return (BrowseDAO) daos.get(BROWSE_DAO);
	}

	public static DAOFactory getInstance()
	{
		return instance;
	}
}
