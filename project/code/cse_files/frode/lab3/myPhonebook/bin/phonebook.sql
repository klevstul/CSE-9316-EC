drop table phonebook_record;
drop table users;
drop sequence users_id;

create sequence users_id;

drop sequence phonebook_record_id;

create sequence phonebook_record_id;

create table users(
	id		integer primary key,
	firstname 	varchar2(20),
	lastname 	varchar2(20),
	access_level 	integer,
	username	varchar2(12) not null,
	password	varchar2(12) not null
);

create table phonebook_record (
	id		integer primary key,
	owner		integer references users(id),
	short_name	varchar2(20),
	full_name	varchar2(200),
	email		varchar2(60),
	contact_number	varchar2(15)
);
