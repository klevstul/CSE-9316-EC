/*
 * file:	UserServiceException.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	02. Nov 2003
 *
 */

package com.freePowder.business;

import com.freePowder.common.NestedException;



public class UserServiceException extends NestedException {


	public UserServiceException(String message) {
		super(message);
	}


	public UserServiceException(String message, Throwable cause) {
		super(message, cause);
	}


	public UserServiceException(Throwable cause) {
		super(cause);
	}

}
