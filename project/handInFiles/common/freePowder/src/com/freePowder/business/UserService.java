/*
 * file:	UserService.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	02. Nov 2003
 *
 */

package com.freePowder.business;

import java.util.List;

import com.freePowder.beans.UsrBean;



public interface UserService {

	UsrBean login(String username, String password) throws UserLoginFailedException;
	void addUser(UsrBean bean) throws UserServiceException;

}
