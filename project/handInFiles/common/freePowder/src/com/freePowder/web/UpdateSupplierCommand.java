/*
 * file:	UpdateSupplierCommand.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	25. Oct 2003
 *
 */

package com.freePowder.web;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;




public class UpdateSupplierCommand implements Command {
	
	public UpdateSupplierCommand() {
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("UpdateSupplierCommand()");
		out.close();	
		return null;
	}
	
}
