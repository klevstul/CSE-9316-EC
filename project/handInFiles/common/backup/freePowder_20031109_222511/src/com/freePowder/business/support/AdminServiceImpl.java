/*
 * file:	AdminServiceImpl.java
 * author:	Frode Klevstul (frode@klevstul.com)
 * started:	08. Nov 2003
 *
 */

package com.freePowder.business.support;

import java.util.List;
//import java.net.URL;
//import java.net.MalformedURLException;
//import java.net.HttpURLConnection;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
//import java.util.Properties;

import com.freePowder.beans.SupplierBean;
import com.freePowder.business.AdminService;
import com.freePowder.business.AdminServiceException;
import com.freePowder.dao.DAOFactory;
import com.freePowder.dao.DataAccessException;
import com.freePowder.dao.SupplierDAO;

import com.freePowder.utility.Debug;


public class AdminServiceImpl implements AdminService {

	private SupplierDAO supplierDao;
	private Debug d;

	public AdminServiceImpl() {
		super();
		supplierDao = DAOFactory.getInstance().getSupplierDAO();
		d = new Debug("AdminServiceImpl()");
	}


	public List getSuppliers() throws AdminServiceException {
		d.print("getSuppliers()");
		try {
			return supplierDao.getSuppliers();
		} catch (DataAccessException e) {
			throw new AdminServiceException("Unable to find suppliers", e);
		}
	}


	public SupplierBean getSupplier(int supplier_pk) throws AdminServiceException {
		d.print("getSupplier(pk)");
		try {
			return supplierDao.getSupplier(supplier_pk);
		} catch (DataAccessException e) {
			throw new AdminServiceException("Unable to find supplier", e);
		}
	}


	public boolean updateDatabase(String xml_url) throws AdminServiceException {
		d.print("updateDatabase("+xml_url+")");


       try {
            
	    // run the Unix "ps -ef" command
            String s = null;

            Process p = Runtime.getRuntime().exec("ls");
            
            BufferedReader stdInput = new BufferedReader(new 
                 InputStreamReader(p.getInputStream()));

            BufferedReader stdError = new BufferedReader(new 
                 InputStreamReader(p.getErrorStream()));

            // read the output from the command
            
            System.out.println("Here is the standard output of the command:\n");
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
            }
            
            // read any errors from the attempted command

            System.out.println("Here is the standard error of the command (if any):\n");
            while ((s = stdError.readLine()) != null) {
                System.out.println(s);
            }
            
            //System.exit(0);
        }
        catch (IOException e) {
            System.out.println("exception happened - here's what I know: ");
            e.printStackTrace();
            //System.exit(-1);
        }


		/*
		try {

			//Properties props = System.getProperties();
			//props.put("http.proxyHost", "www-proxy.cse.unsw.edu.au");
			//props.put("http.proxyPort", "proxyhostport");
			//props.put("http.proxyUser", "proxyhostport");
			//props.put("http.proxyPassword", "proxyhostport");
			//System.setProperties(props);
			
 			//URL url = new URL(xml_url);
 			URL url = new URL("http://www.klevstul.com");
 			HttpURLConnection httpUrlConnection = new HttpURLConnection(url);
 			httpUrlConnection.connect();
 			
 			long timestamp_long = httpUrlConnection.getDate();
 			String contentType = httpUrlConnection.getContentType();
 			
 			d.print(httpUrlConnection.toString()+" "+"timestamp:"+timestamp_long+" contentType:"+contentType);
 			

 		} catch (MalformedURLException e) {
 			d.print("exception: "+e.getMessage());
 		} catch (IOException e) {
 			d.print("exception: "+e.getMessage());
 		}
 		*/

		
		//if (1==2)
		//	throw new AdminServiceException("Unable to update database", null);
			
		return true;
		
	}

}
