<%@ page contentType="text/html" import="com.freePowder.beans.*,java.util.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>-   Freepowder.com   -</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body bgcolor="#415464" background="images/bg.gif" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<% // Insert the shopping cart section %>
    <jsp:include page="cart.jsp" />
    
<% // Insert the header section %>
    <jsp:include page="header.jsp" />
    
<% // Check which page we are on %>
<% if(request.getParameter("page") == null) { %>
    <jsp:include page="home.jsp" />
<% } else if(request.getParameter("page").equals("browse")){ %>
    <jsp:include page="browse.jsp" />
<% } else if(request.getParameter("page").equals("mydetails")){ %>
    <jsp:include page="mydetails.jsp" />
<% } else if(request.getParameter("page").equals("cart")){ %>
    <jsp:include page="cart.jsp" />
<% } else if(request.getParameter("page").equals("contact")){ %>
    <jsp:include page="contact.jsp" />
<% } else if(request.getParameter("page").equals("newuser")){ %>
    <jsp:include page="newuser.jsp" />
<% } else if(request.getParameter("page").equals("home")){ %>
    <jsp:include page="home.jsp" />
<% } else { %>
    <jsp:include page="home.jsp" />
<% } %>


</body>
</html>
